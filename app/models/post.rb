class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	
	validates_presence_of :title
	validates_length_of :title, :in => 1..50
	
	validates_presence_of :body

end
